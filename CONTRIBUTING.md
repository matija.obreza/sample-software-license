# Contributing to Community GRIN-Global Software Project

Thank you for considering contributing to the project. Prior to making any
contribution please read the [Your contribution section](#contributing),
[LICENSE](LICENSE), [DISCLAIMER](DISCLAIMER) and [README](README.md) information
 contained in this repository.

# Ways to contribute

## Documentation

Help us develop the project documentation! While we have documented most of what
GRIN-Global does and how it does it, there are always things that remain
undocumented. Check out the documentation pages and add NEW STUFF!!

## Report issues and suggestions

Before you jump into programming and developing new features for GRIN-Global you
should check whether someone else is already working on that feature by checking
the issues list at https://gitlab.com/groups/GRIN-Global/issues.
If your feature or problem is not listed, register a new issue!

## Software engineering

Develop new features...

# <a name="contributing"></a> Your contribution

By contributing to the GRIN-Global software project you warrant that you own
and/or are entitled to authorize any work incorporating the contribution to be
licensed under Apache License, Version 2.0 (the [LICENSE](LICENSE)) and certain
information ([DISCLAIMER](DISCLAIMER)) which is required to be included in all
copies of the original GRIN-Global software and any modifications thereto.

## License notice in source code

Every program source code file **must** include the following header:

```java
/*
 * This software was developed by the GRIN-Global Community. Access, use and
 * modification is subject to Apache License, Version 2.0 (available at
 * http://www.apache.org/licenses/LICENSE-2.0) and certain information which is
 * required to be included in all copies the original GRIN-Global software and
 * any modifications thereto (see DISCLAIMER in project root or available at
 * https://gitlab.com/GRIN-Global/project/DISCLAIMER).
 */
```

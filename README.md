Welcome to Community GRIN-Global Software Project!

# Community GRIN-Global Project

The community of international and national genebanks and research organizations
contribute to development and improvement of GRIN-Global, a genebank management
software system.

# Contributing to the project

See our [contribution guide](CONTRIBUTING.md).

# Licensing information

This GRIN-Global software is based on work by USDA/ARS and updated by the
GRIN-Global community. Access, use and modification is subject to Apache
License, Version 2.0 (the [license](LICENSE)) and certain information which is
required to be included in all copies of the original GRIN-Global software and
any modifications thereto ([disclaimer](DISCLAIMER)).
